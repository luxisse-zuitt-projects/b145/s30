// Getting all todos
console.log(fetch('https://jsonplaceholder.typicode.com/todos'));

// Fetch request from JSON Placeholder API
async function fetchData(){

	let result = await fetch('https://jsonplaceholder.typicode.com/todos')
	console.log(result);

	console.log(result.body);

	let json = await result.json();
	console.log(json);

	// Create array using map and return just the title of every item
	let titlesArray = json.map(obj => {
		let rObj = {}
		rObj[obj.id] = obj.title
		return rObj
	})

console.log(titlesArray);

}

fetchData();



// Get a to do list item

fetch('https://jsonplaceholder.typicode.com/todos/111')
.then((response) => response.json())
.then((json) => console.log(json));


console.log('The item "magni accusantium labore et id quis provident" on the list has a status of false');



// Create to do list - POST
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		completed: false,
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));



// Update a to do list item - PUT
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		id: 111,
		title: 'Updated To Do List Item',
		description: 'This is an updated to do list',
		status: 'pending',
		userId: 1
	})

})
.then((response) => response.json())
.then((json) => console.log(json));



// Update a to do list item - PATCH
fetch('https://jsonplaceholder.typicode.com/todos/111', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		description: 'This is an updated to do list',
		status: 'complete',
		dateCompleted: '26/01/22',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));



// Delete a to do item
fetch('https://jsonplaceholder.typicode.com/todos/111', {
	method: 'DELETE'
});